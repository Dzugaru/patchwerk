using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class CSPatchwerk : EditorWindow
{
    #region Structs
    public struct TextureBlock
    {
        public Texture2D Tex;
        public RectInt Rect;

        public TextureBlock(Texture2D tex, RectInt block)
        {
            Tex = tex;
            Rect = block;
        }

        public Color[] GetPixels()
        {
            return Tex.GetPixels(Rect.x, Rect.y, Rect.width, Rect.height);
        }

        public void SetPixels(Color[] ps)
        {
            Tex.SetPixels(Rect.x, Rect.y, Rect.width, Rect.height, ps);
        }

        public TextureBlock GetBlock(RectInt r)
        {
            return new TextureBlock(Tex, new RectInt(Rect.x + r.x, Rect.y + r.y, r.width, r.height));
        }
    }


    public struct MaskBlock
    {
        public float[,] Mask;
        public RectInt Rect;
        public bool IsTransposed;

        public MaskBlock(float[,] mask, RectInt block, bool isTransposed)
        {
            Mask = mask;
            Rect = block;
            IsTransposed = isTransposed;
        }

        public float GetPix(int i, int j)
        {
            if (IsTransposed)
                return Mask[j + Rect.x, i + Rect.y];
            else
                return Mask[i + Rect.y, j + Rect.x];
        }

        public void SetPix(int i, int j, float p)
        {
            if (IsTransposed)
                Mask[j + Rect.x, i + Rect.y] = p;
            else
                Mask[i + Rect.y, j + Rect.x] = p;
        }

        public static MaskBlock WhiteMask(RectInt size)
        {
            var m = new MaskBlock(new float[size.height, size.width], size, false);
            for (int i = 0; i < size.height; i++)
                for (int j = 0; j < size.width; j++)
                    m.Mask[i, j] = 1f;
            return m;
        }

        public MaskBlock GetBlock(RectInt r)
        {
            return new MaskBlock(Mask, new RectInt(Rect.x + r.x, Rect.y + r.y, r.width, r.height), IsTransposed);
        }
    }
    #endregion

    #region Fields
    public Texture2D sourceTexture;
    public Material previewMaterial;

    // Should we make resulting quilt texture tileable.
    // If so, we will make a special quilt with the same blocks repeated on left-right and top-bottom sides
    // and cut the texture on center of those blocks (TODO: see picture)
    public bool makeTiled = true;

    // Resulting patchwerk size
    public int resultSize = 1024;

    // Distance between centers of quilted tiles,
    // must be power of 2 (because the final tiled texture must be power of 2
    // and will be cut from X marks (TODO: draw picture)
    public int stepSize = 256;

    // Overlap in percents from step size (0-50%), for GUI
    float overlapPercent = 25;

    // Total overlap between quilted tiles
    int overlap; 

    // How much we blur quilt masks in the direction perpendicular to the seam direction 
    // (vertical or horizontal), setting this high will make the transition cut smoother
    public float seamSmooth = 0; // 0-1 ?

    // Random seed to repeat exact same block choice
    public int seed;

    public bool debugDrawStitches = false;

    // Source texture block size (this is equal to stepSize + overlap
    // since we need half overlap margin from each side)
    int blockSize;

    // How many tiles we need in each direction
    int tileCountX, tileCountY;

    // Current tile coords
    int tileX, tileY;
    bool isFinished;

    // Blocks saved for making tileable final texture,
    // we'll repeat those blocks on bottom and right side
    List<TextureBlock> topBlocks, leftBlocks;

    // Quilted texture (bigger than final result by stepSize,
    // we will cut stepSize / 2 from each side so the result texture seamlessly tiles)
    Texture2D quilt;

    GUIStyle errorLabelStyle;

    int? prevSeed;
    int progress;

    bool triedAtLeastOnce;

    bool showAdvanced;
    string tryTextureName = "__patchwerk_try.png";
    #endregion

    private void Awake()
    {
        errorLabelStyle = new GUIStyle();
        errorLabelStyle.normal.textColor = Color.red;
        errorLabelStyle.wordWrap = true;
        seed = Random.Range(0, int.MaxValue);

        minSize = new Vector2(500, 500);
    }

    static void LaunchWindow()
    {
        var tex2D = (Texture2D)Selection.activeObject;
        CSPatchwerk window = (CSPatchwerk)EditorWindow.GetWindow(typeof(CSPatchwerk));
        window.sourceTexture = tex2D;
        window.titleContent = new GUIContent("Patchwerk");
        window.Show();
    }

    [MenuItem("Window/Patchwerk", priority=1000000)]
    [MenuItem("Assets/Patchwerk", priority=1000000)]
    static void Patchwork()
    {
        LaunchWindow();
    }

    [MenuItem("Assets/Patchwerk", validate = true)]
    static bool PatchworkValidate()
    {        
        return Selection.activeObject.GetType() == typeof(Texture2D);
    }

    void OnGUI()
    {
        //TODO: tooltips        
        var resultSizes = new[] { 256, 512, 1024, 2048, 4096, 8192 };
        var stepSizes = new[] { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };

        sourceTexture = (Texture2D)EditorGUILayout.ObjectField("Source texture", sourceTexture, typeof(Texture2D), allowSceneObjects: false);

        if (sourceTexture == null)
            GUI.enabled = false;

        var minTexSize = int.MaxValue;
        if (sourceTexture != null)
            minTexSize = Mathf.Min(sourceTexture.width, sourceTexture.height);

        resultSize = EditorGUILayout.IntPopup("Result Size", resultSize,
                                            resultSizes.Select(x => x.ToString()).ToArray(),
                                            resultSizes);

        stepSizes = stepSizes.Where(x => x < resultSize && x < minTexSize).ToArray();
        if (stepSize >= resultSize)
            stepSize = resultSize / 2;
        stepSize = EditorGUILayout.IntPopup("Patch Size", stepSize,
                                            stepSizes.Select(x => x.ToString()).ToArray(),
                                            stepSizes);

        overlapPercent = EditorGUILayout.Slider("Patch Overlap %", overlapPercent, 0f, 50f);
        overlap = Mathf.RoundToInt(stepSize * 0.01f * overlapPercent);
        if (overlap < 4)
            overlap = 4;
        if (overlap % 2 == 1)
            overlap++;

        EditorGUILayout.LabelField("Overlap px", overlap.ToString());

        if (stepSize + overlap > minTexSize)
        {
            EditorGUILayout.LabelField("Patch Size + Overlap is bigger than source texture,", errorLabelStyle);
            EditorGUILayout.LabelField("please reduce either one", errorLabelStyle);
            GUI.enabled = false;
        }

        makeTiled = EditorGUILayout.Toggle("Make seamlessly tileable", makeTiled);

        
        seed = EditorGUILayout.IntField("Seed", seed);
        EditorGUILayout.SelectableLabel("Previous seed: " + (prevSeed.HasValue ? prevSeed.ToString() : "---"));

        showAdvanced = EditorGUILayout.Foldout(showAdvanced, "Advanced settings");
        if (showAdvanced)
        {
            seamSmooth = EditorGUILayout.Slider("Seam smooth factor", seamSmooth, 0f, 1f);
            tryTextureName = EditorGUILayout.TextField("Try result texture name", tryTextureName);
            debugDrawStitches = EditorGUILayout.Toggle("[DEBUG] Show stitches", debugDrawStitches);
        }

        if (sourceTexture == null)
        {
            GUILayout.Button("Work");
            return;
        }

        bool isWrongTexType = false;
        bool isNotReadable = false;

        string assetPath = AssetDatabase.GetAssetPath(sourceTexture);
        var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        isWrongTexType = tImporter.textureType != TextureImporterType.Sprite;
        isNotReadable = !tImporter.isReadable;

        if (isWrongTexType)
            EditorGUILayout.LabelField("Texture Type is not Sprite", errorLabelStyle);

        if (isNotReadable)
            EditorGUILayout.LabelField("Texture is not Readable", errorLabelStyle);

        if (isWrongTexType || isNotReadable)
            EditorGUILayout.LabelField("Pressing \"Try\" will change your texture to fix errors above", errorLabelStyle);

        previewMaterial = (Material)EditorGUILayout.ObjectField("Preview material", previewMaterial, typeof(Material), false);

        if (GUILayout.Button("Try", GUILayout.Height(50)))
        {
            GUI.FocusControl(null);
            progress = 0;
            EditorUtility.DisplayProgressBar("Progress", "Stitching...", 0);
            Random.InitState(seed);

            if (isWrongTexType)
            {
                tImporter.textureType = TextureImporterType.Sprite;
                AssetDatabase.ImportAsset(assetPath);
            }

            if (isNotReadable)
            {
                tImporter.isReadable = true;
                AssetDatabase.ImportAsset(assetPath);
            }

            AssetDatabase.Refresh();

            isFinished = false;
            Quilt(resultSize, resultSize);

            while (!isFinished)
                if (!Step())
                    isFinished = true;

            var resTex = new Texture2D(resultSize, resultSize, TextureFormat.RGBA32, true);
            var offset = makeTiled ? (overlap + stepSize) / 2 : overlap / 2;
            resTex.SetPixels(quilt.GetPixels(offset, offset, resultSize, resultSize));

            File.WriteAllBytes(Path.Combine(Directory.GetParent(Application.dataPath).ToString(), 
                                            Path.GetDirectoryName(assetPath), 
                                            tryTextureName), resTex.EncodeToPNG());
            AssetDatabase.Refresh();

            if (previewMaterial != null)
                previewMaterial.mainTexture = (Texture)AssetDatabase.LoadMainAssetAtPath(Path.Combine(Path.GetDirectoryName(assetPath), tryTextureName).Replace('\\', '/'));

            prevSeed = seed;
            seed = Random.Range(0, int.MaxValue);

            EditorUtility.ClearProgressBar();

            triedAtLeastOnce = true;
        }

        if (!triedAtLeastOnce)
            GUI.enabled = false;

        if (GUILayout.Button("Save try result", GUILayout.Height(50)))
        {
            var sourceTexName = Path.GetFileNameWithoutExtension(assetPath);
            var srcPath = Path.Combine(Path.GetDirectoryName(assetPath), tryTextureName).Replace('\\', '/');
            AssetDatabase.RenameAsset(srcPath, sourceTexName + "_pw_" + prevSeed.ToString() + ".png");
            AssetDatabase.Refresh();
        }
    }

    void Quilt(int dstH, int dstW)
    {        
        // TODO: see reference figure
        blockSize = stepSize + overlap;
       
        tileCountY = dstH / stepSize;
        tileCountX = dstW / stepSize;
        if(makeTiled)
        {
            tileCountX++;
            tileCountY++;
            topBlocks = new List<TextureBlock>();
            leftBlocks = new List<TextureBlock>();
        }
        tileX = 0;
        tileY = 0;

        var quiltSizeX = dstW + overlap;
        var quiltSizeY = dstH + overlap;
        if(makeTiled)
        {
            quiltSizeX += stepSize;
            quiltSizeY += stepSize;
        }
        
        quilt = new Texture2D(quiltSizeX, quiltSizeY, TextureFormat.RGBA32, false);
    }

    bool Step()
    {        
        // Select source block
        TextureBlock block;
        if (makeTiled)
        {
            // Repeat blocks on right and bottom sides
            if (tileX == tileCountX - 1)
            {
                if(tileY == tileCountY - 1)                
                    block = leftBlocks[0];                
                else
                    block = leftBlocks[tileY];
            }
            else if (tileY == tileCountY - 1)
            {
                block = topBlocks[tileX];
            }
            else
            {
                block = GetRandomBlock();
                if (tileY == 0)
                    topBlocks.Add(block);
                if (tileX == 0)
                    leftBlocks.Add(block);
            }
        }
        else
        {
            block = GetRandomBlock();
        }

        // Get stitched (modified) areas of existing quilt texture
        TextureBlock? top, left;
        if (tileY == 0) top = null;
        else top = new TextureBlock(quilt, new RectInt(stepSize * tileX, stepSize * tileY, blockSize, overlap));

        if (tileX == 0) left = null;
        else left = new TextureBlock(quilt, new RectInt(stepSize * tileX, stepSize * tileY, overlap, blockSize));

        // Quilt
        QuiltTile(top, left, block);        

        // Advance tile indices
        tileX++;
        if (tileX >= tileCountX)
        {
            tileX = 0;
            tileY++;
            if (tileY >= tileCountY) return false;
        }

        progress++;
        EditorUtility.DisplayProgressBar("Progress", "Stitching...", (float)progress / (tileCountX * tileCountY));
        return true;
    }

    TextureBlock GetRandomBlock()
    {        
        int x = Random.Range(0, sourceTexture.width - blockSize + 1);
        int y = Random.Range(0, sourceTexture.height - blockSize + 1);
        return new TextureBlock(sourceTexture, new RectInt(x, y, blockSize, blockSize));
    }

    void QuiltTile(TextureBlock? top, TextureBlock? left, TextureBlock block)
    {   
        var bTop = block.GetBlock(new RectInt(0, 0, blockSize, overlap));
        var bLeft = block.GetBlock(new RectInt(0, 0, overlap, blockSize));

        //Fixing up
        MaskBlock maskTop;        
        if (top != null)
            maskTop = GetMinCutMask(top.Value, bTop, false, out var err);                    
        else
            maskTop = MaskBlock.WhiteMask(new RectInt(0, 0, blockSize, overlap));

        var maskTopRight = maskTop.GetBlock(new RectInt(overlap, 0, stepSize, overlap));
        MaskBlock maskTopLeft = maskTop.GetBlock(new RectInt(0, 0, overlap, overlap));

        var bTopRight = bTop.GetBlock(new RectInt(overlap, 0, stepSize, overlap));
        var topRight = new TextureBlock(quilt, new RectInt(tileX * stepSize + overlap, tileY * stepSize, stepSize, overlap));
        if(debugDrawStitches)
            BlendByMaskDebug(topRight, bTopRight, maskTopRight);
        else
            BlendByMask(topRight, bTopRight, maskTopRight);

        //Fixing left
        MaskBlock maskLeft;
        if (left != null)        
            maskLeft = GetMinCutMask(left.Value, bLeft, true, out var err);
        else
            maskLeft = MaskBlock.WhiteMask(new RectInt(0, 0, overlap, blockSize));

        var maskLeftBottom = maskLeft.GetBlock(new RectInt(0, overlap, overlap, stepSize));
        var maskLeftTop = maskLeft.GetBlock(new RectInt(0, 0, overlap, overlap));
        var bLeftBottom = bLeft.GetBlock(new RectInt(0, overlap, overlap, stepSize));
        var leftBottom = new TextureBlock(quilt, new RectInt(tileX * stepSize, tileY * stepSize + overlap, overlap, stepSize));
        if (debugDrawStitches)
            BlendByMaskDebug(leftBottom, bLeftBottom, maskLeftBottom);
        else
            BlendByMask(leftBottom, bLeftBottom, maskLeftBottom);

        // Get corner mask then fixing corner
        MultiplyMasks(maskTopLeft, maskLeftTop);                
        
        var bTopLeft = block.GetBlock(new RectInt(0, 0, overlap, overlap));
        var topLeft = new TextureBlock(quilt, new RectInt(tileX * stepSize, tileY * stepSize, overlap, overlap));
        if (debugDrawStitches)
            BlendByMaskDebug(topLeft, bTopLeft, maskTopLeft);
        else
            BlendByMask(topLeft, bTopLeft, maskTopLeft);

        //Drawing center
        var bBottomRight = block.GetBlock(new RectInt(overlap, overlap, stepSize, stepSize));
        var bottomRight = new TextureBlock(quilt, new RectInt(tileX * stepSize + overlap, tileY * stepSize + overlap, stepSize, stepSize));
        CopyTextureBlock(bBottomRight, bottomRight);        
    }

    void BlendByMask(TextureBlock dst, TextureBlock src, MaskBlock mask)
    {
        var dstPix = dst.GetPixels();
        var srcPix = src.GetPixels();       
        var s = mask.Rect.width;

        var resPix = new Color[mask.Rect.height * mask.Rect.width];

        for (int i = 0; i < mask.Rect.height; i++)
        {
            for (int j = 0; j < mask.Rect.width; j++)
            {
                var m = mask.GetPix(i, j);
                resPix[s * i + j] = srcPix[s * i + j] * m + dstPix[s * i + j] * (1 - m);
            }
        }

        dst.SetPixels(resPix);       
    }

    // Debug draw stitches with magenta line
    void BlendByMaskDebug(TextureBlock dst, TextureBlock src, MaskBlock mask)
    {
        var dstPix = dst.GetPixels();
        var srcPix = src.GetPixels();
        var s = mask.Rect.width;

        var resPix = new Color[mask.Rect.height * mask.Rect.width];

        for (int i = 0; i < mask.Rect.height; i++)
        {
            for (int j = 0; j < mask.Rect.width; j++)
            {
                var m = mask.GetPix(i, j);
                resPix[s * i + j] = srcPix[s * i + j] * m + dstPix[s * i + j] * (1 - m);
                if (i > 0 && j > 0)
                {
                    // draw mask ~0.5f point (center) as magenta
                    var m1 = mask.GetPix(i - 1, j);
                    var m2 = mask.GetPix(i, j - 1);                    
                    if (m < 0.5f && m1 >= 0.5f || m > 0.5f && m1 <= 0.5f ||
                        m < 0.5f && m2 >= 0.5f || m > 0.5f && m2 <= 0.5f)
                    {
                        resPix[s * i + j] = Color.magenta;
                        resPix[s * (i - 1) + j] = Color.magenta;
                        resPix[s * i  + j - 1] = Color.magenta;
                    }
                }                
            }
        }

        dst.SetPixels(resPix);
    }

    void CopyTextureBlock(TextureBlock src, TextureBlock dst)
    {
        dst.SetPixels(src.GetPixels());
    }

    // NOTE: Writes back to m1
    void MultiplyMasks(MaskBlock m1, MaskBlock m2)
    {
        for (int i = 0; i < m1.Rect.height; i++)        
            for (int j = 0; j < m1.Rect.width; j++)            
                m1.SetPix(i, j, m1.GetPix(i, j) * m2.GetPix(i, j));
    }

    float[,] OverlapErrorSurface(TextureBlock b1, TextureBlock b2, bool isVertical)
    {
        var res = new float[blockSize, overlap];
        var b1Pix = b1.GetPixels();
        var b2Pix = b2.GetPixels();
        var s = b1.Rect.width;

        for (int i = 0; i < b1.Rect.height; i++)
        {
            for (int j = 0; j < b1.Rect.width; j++)
            {
                var p1 = b1Pix[s * i + j];
                var p2 = b2Pix[s * i + j];
                var diff = (p1.r - p2.r) * (p1.r - p2.r) + (p1.b - p2.b) * (p1.b - p2.b) + (p1.g - p2.g) * (p1.g - p2.g) + (p1.a - p2.a) * (p1.a - p2.a);

                if (isVertical)
                    res[i, j] = diff;
                else
                    res[j, i] = diff;
            }
        }

        return res;       
    }

    MaskBlock GetMinCutMask(TextureBlock b1, TextureBlock b2, bool isVertical, out float cutErr)
    {
        cutErr = 0;

        var mins = new float[blockSize + 1, overlap + 2];
        for (int i = 0; i < blockSize; i++)
            mins[i + 1, 0] = mins[i + 1, overlap + 1] = float.MaxValue;

        var mData = OverlapErrorSurface(b1, b2, isVertical);
        var mMask = new float[blockSize, overlap];

        //Fill mins
        for (int i = 0; i < blockSize; i++)
        {
            for (int j = 0; j < overlap; j++)
            {
                float e = mData[i, j];
                mins[i + 1, j + 1] = e + Mathf.Min(mins[i, j], mins[i, j + 1], mins[i, j + 2]);
            }
        }

        //Backtrack and fill mask  
        int x = 0;
        float min = float.MaxValue;
        for (int j = 0; j < overlap; j++)
            if (mins[blockSize, j + 1] < min)
            {
                min = mins[blockSize, j + 1];
                x = j;
            }

        cutErr = min;
        for (int i = blockSize - 1; i >= 0; i--)
        {
            if (seamSmooth == 0)
            {
                for (int j = x; j < overlap; j++)
                    mMask[i, j] = 1.0f;
            }
            else
            {
                for (int j = 0; j < overlap; j++)
                    mMask[i, j] = 1f / (1 + Mathf.Exp(-(j - x) / (seamSmooth * overlap)));
            }

            float l = mins[i, x];
            float c = mins[i, x + 1];
            float r = mins[i, x + 2];

            if (l < c && l < r) x--;
            else if (r < c && r < l) x++;
        }

        var maskDim = isVertical ? new RectInt(0, 0, overlap, blockSize) : new RectInt(0, 0, blockSize, overlap);
        return new MaskBlock(mMask, maskDim, !isVertical);
    }
}
