# Patchwerk
### _using one photo quickly iterate various seamless texture designs_

_Example photos are from FreeStockTextures.com and Pexels.com_

![Patchwerk](Assets/CodeSorcery/Patchwerk/Readme/raw_to_seamless.png)


# What is it?
Patchwerk is a tool that allows you to quickly make a seamlessly tileable texture from a photo right inside Unity3D editor. 

It works by taking random blocks from a source photo and stitching them together in a patchwork blanket fashion.
Since the source blocks are completely random (not tied to any grid), it allows you to quickly iterate through various designs
that include or exclude various regions of the photo. Try several times - and you can generate a great seamless texture free of tiling artifacts like repeating patterns that stand out too much or visible seams. 

One more advantage of Patchwerk is that by default it doesn't use alpha blending for stitching like some other automating seamless texture generation tools (or texture bombing). It's based on minimum pixel cut algorithm, described in a whitepaper [Wang Tiles for Image and Texture Generation](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.8.9049&rep=rep1&type=pdf). It works like this - for any two overlapping blocks we find a cut that minimizes the difference between two pixel colors that reside on this cut. This produces great highly customized to particular image cuts without any alpha blending:

![Cuts](Assets/CodeSorcery/Patchwerk/Readme/cuts.png)

_Cuts follow objects in the image_

# Caveats
This tool works best with evenly illuminated organic textures. Photos with light gradient from top to bottom or vignette may produce visible
checkerboard patterns because random blocks will be lighter or darker. The tool may include block automatic gamma correction in the future, for now you have to fix illumination gradient or crop the photo beforehand.

Some man-made patterns like bricks or wood planks will be stitched in a "wrong" way. This may be acceptable if the texture is small and in background. The tool may include more block selection algorithms in the future to battle this.

# How to use?
Copy `Assets/CodeSorcery/Patchwerk` folder to your Unity project. 

Patchwerk adds a context menu to texture assets (in the bottom) and in Window main menu (again, in the bottom).

Quickstart:
1) Right click a texture asset, choose Patchwerk menu item in the bottom
2) Drag a material, that is used somewhere in your scene to quickly preview results (or make a 3D plane object and assign some material with tiling 2 to it (to check seamless tiling), or use Patchwerk/Examples/Scenes/PatchwerkTexView scene)
3) Modify generation parameters
4) Click "Try" several times until you like the result (Patchwerk will generate `__patchwerk_try.png` texture and assign it as main texture to preview material selected)
5) Click "Save", the previous try, that is displayed now in preview material will be saved to `<texture_name>_pw_<seed>.png`

Parameters:
- **Source texture**

Add a source photo/texture here. Source texture should be imported as a Sprite (to avoid Unity power of 2 size coercion) and Readable (Read/Write checked). However, it's not required that you do this beforehand, since "Try" button will fix texture import type and make it readable for you anyway.
- **Result size**

Resulting patchwork texture size.
- **Patch size**

Source patch (block) size. The algorithm will take blocks of this size from the source, if you make them too small the patchwork texture will be too fragmented, if you make them too big - the variety of the result will suffer. In general - this size depends on your source size and the size of image features (objects) in the source - try different values! Block size cannot be greater than half of Result size (minimum reasonable quilt is 2x2).
- **Patch Overlap %**

This parameter controls overlap amount between blocks in resulting patchwork texture. If you make it too small - the cuts will be constrained to vertical and horizontal straight lines which can give a checkerboard look to the result. If you make it too big the cuts will be more random, but this can produce artifacts. 25% is a good starting point.
- **Overlap px**

This just shows precise overlap between blocks in pixels, controlled by the Overlap Percent slider above. Minimum overlap is 4 pixels and it must be even.
- **Make seamlessly tileable**

Unchecking this will make a texture, that doesn't tile seamlessly. The blocks at left-right and top-bottom sides will no longer match, which gives a little bit more variety if you don't really need tileable texture.
- **Seed**

Random seed that controls block choice. If the parameters (source size, result size, patch size, overlap and seed) are the same - it will choose exact same blocks for generation. This can be useful if you have several matched textures/maps and want to patchwork them all in an exact same way. Seed will change on every "Try" button click for convenience.
- **Previous seed**

This just shows previous try seed, if you like the try and want to save it. The label is selectable.
- **Preview material**

Drag a material from your scene, every try you make will update main texture of this material. This allows you to quickly iterate designs.
- **Try**

Generate one patchwork texture, the resulting texture will be saved to `__patchwerk_try.png` in the same folder that your source texture and
assigned to Preview Material main texture. Click "Try" until you find the design you like.
- **Save**

Click this to save last try you did (that is shown on Preview Material). This will save the try to `<texture_name>_pw_<seed>.png` in the same folder that your source is. You can use the seed in the saved filename to recreate the result (for example if you make some modifications to source texture (don't change the source texture size, however)).

# Some more examples 
_Note, the result is presented tiled 2x2 to show that the resulting texture is indeed seamless. Since each image in result column is 4 identical textures side by side you can see some regularities present._

| Source | Result, tiled 2x2 |
| ---    | ---   |
| <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/pexels-digital-buggu-186230_1.jpg" alt="before" width="300"/> | <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/pexels-digital-buggu-186230_pw_58446749.jpg" alt="after" width="500"/> |
| <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/pexels-msvr-997704_1.jpg" alt="before" width="300"/> | <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/pexels-msvr-997704_pw_264473599.jpg" alt="after" width="500"/> |
| <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/pexels-irina-iriser-1408221_1.jpg" alt="before" width="300"/> | <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/pexels-irina-iriser-1408221_pw_615943400.jpg" width="500"/> |
| <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/unknown.jpg" alt="before" width="300"/> | <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/unknown_pw_771210309.jpg" width="500"/> |
| <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/unknown2.jpg" alt="before" width="300"/> | <img src="Assets/CodeSorcery/Patchwerk/Readme/SmallExamples/unknown_pw_2057495791.jpg" width="500"/> |












